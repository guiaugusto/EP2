package model;

public class Estoque extends Produto{

	private int quantidade;
	
	public Estoque(){
	}
		
	public int getQuantidade(){
		return quantidade;
	}
	
	public void setQuantidade(int quantidade){
		this.quantidade = quantidade;
	}
	
	
}
