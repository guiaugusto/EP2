package model;

public class Produto extends Pedido{

	private String tipo;
	private double valor;
	
	public Produto(){
	}
	
	public String getTipo(){
		return tipo;
	}
	
	public void setTipo(String tipo){
		this.tipo = tipo;
	}
	
	public double getValor(){
		return valor;
	}
	
	public void setValor(double valor){
		this.valor = valor;
	}

	
}
