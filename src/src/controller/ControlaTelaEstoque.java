package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import view.TelaEstoque;
import model.Estoque;

public class ControlaTelaEstoque extends Estoque{

	public TelaEstoque telaestoque;
	public ControlaNovoItem novoitem;
	
	public ControlaTelaEstoque(){
		
		telaestoque = new TelaEstoque();
		AdicionarEstoque();
		ManipulaBotao1(telaestoque.botao1);
		ManipulaBotao2(telaestoque.botao2);
		ManipulaBotao3(telaestoque.botao3);
	}
	
	public void AdicionarEstoque(){
		
		try {
			leitor();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
		public void ManipulaBotao1(JButton botao1){
		
			botao1.addActionListener(new ActionListener(){

				public void actionPerformed(ActionEvent arg0) {
									
					String inputValue = JOptionPane.showInputDialog("Insira a quantidade a ser adicionada");
					
					if(inputValue != null){
					
						int valor = Integer.valueOf(inputValue);

						try {
							escrever(valor);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			});
			
		}
	

		public void ManipulaBotao2(JButton botao2){
			
			botao2.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0){
		
					telaestoque.dispose();
					
				}
			});
			
		}
		
		public void ManipulaBotao3(JButton botao3){
				
			botao3.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0){
			
					new ControlaNovoItem();
					
				}	
			});
			
		}

		
	public void leitor() throws IOException{
		
		BufferedReader buffRead = new BufferedReader(new FileReader("doc/estoquebebidas.txt"));
		
		String linhaaux = "";
		String[] dados = new String[3];	
		
		//int tamanho;
		
		int i = 0;
		
		String linha = null;
		
		while(buffRead.ready()){
		
			if(!linhaaux.equals(linha)){
				linhaaux = buffRead.readLine();
				
				if(linhaaux != ""){
				
					dados = linhaaux.split(";");
					
						telaestoque.model.add(i, dados[0]);
						i++;
						
				}	
			}
		}
			
		buffRead.close();
		
		
	}

	public void escrever(int valor) throws IOException{
			
			BufferedReader buffRead = new BufferedReader(new FileReader("doc/estoquebebidas.txt"));

			String linhaaux = "";
			String[] dados;	
			String[] dadosaux;
			String linhaescolhida = null;
			
			
			int i = 0, n = 0, escolhido = 0;
			
			String[] linha = {""};
			
			while(buffRead.ready()){
				
				linha[i] = buffRead.readLine();
				dadosaux = linha[i].split(";");
				linhaaux = telaestoque.lista.getSelectedValue();
				dados = linhaaux.split("");
				dadosaux = linha[i].split(";");
				if(dados[0].equals(dadosaux[0])){
				linhaescolhida = linha[i];
				escolhido = i;
				}
			
				
				i++;
				
			}
			buffRead.close();

			BufferedWriter buffWrite = new BufferedWriter(new FileWriter("doc/estoquebebidas.txt"));

			
			linhaaux = telaestoque.lista.getSelectedValue();
			System.out.println(linhaaux);
			dados = linhaaux.split("-");
			
				
				if(n == escolhido){
				buffWrite.append(linhaescolhida + "\n");
				}
				else{
				buffWrite.append(linha[n] + "\n");
				}
				
				n++;
			
		
			buffWrite.close();
			
		}

	
}
