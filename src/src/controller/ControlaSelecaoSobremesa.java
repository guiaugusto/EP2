package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;

import model.Sobremesa;

import view.TelaSelecaoRefeicao;
import view.TelaSelecaoSobremesa;

public class ControlaSelecaoSobremesa extends Sobremesa{

	public TelaSelecaoSobremesa telasobremesa;
	public ControlaTelaMenu tela_menu;
	
	
	public ControlaSelecaoSobremesa(){
		
		telasobremesa = new TelaSelecaoSobremesa();
		try {
			leitor();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ManipulaComboBox(telasobremesa.sobremesa_opcao);
		ManipulaBotao1(telasobremesa.botao1);
		ManipulaBotao2(telasobremesa.botao2);
	
		
	}
	
	public void ManipulaComboBox(JComboBox<String> sobremesa_opcao){
		sobremesa_opcao.addActionListener(new ActionListener() {  
			  public void actionPerformed(ActionEvent evt) {  
			    
				String refeicao;
				  
				refeicao = telasobremesa.sobremesa_opcao.getSelectedItem().toString();
				telasobremesa.area.setText(telasobremesa.area.getText()+ refeicao + "\n");
				
			  }  
			});  
		
	}
	
	public void ManipulaBotao1(JButton botao1){
		botao1.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
			
				setListaPedido(telasobremesa.area.getText());
				telasobremesa.dispose();
			}});
		
	}
	
	
	public void ManipulaBotao2(JButton botao2){
	botao2.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent arg0) {

			telasobremesa.dispose();
			
		}});
	}

	public void leitor() throws IOException{
		
		
		BufferedReader buffRead = new BufferedReader(new FileReader("doc/estoquesobremesas.txt"));
		
		String linha = " ";
		String linhaaux = "";
		String[] dados;
		
		while(buffRead.ready()){
		
			if(!linhaaux.equals(linha)){
				linhaaux = buffRead.readLine();
				dados = linhaaux.split(";");
				
				telasobremesa.sobremesa_opcao.addItem(dados[0]);
				
			}
			else{
				
				break;
			}
			
		}
		
		buffRead.close();
		
		
	}

	
}
