package controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import model.Pedido;
import model.Refeicao;

import view.TelaMenu;
import view.TelaSelecaoRefeicao;


public class ControlaSelecaoRefeicao extends Refeicao{

	public TelaSelecaoRefeicao telarefeicao;
	public ControlaTelaMenu tela_menu;
	public Pedido pedido;
	
	public ControlaSelecaoRefeicao(){
		
		telarefeicao = new TelaSelecaoRefeicao();
		pedido = new Pedido();
		try {
			leitor();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ManipulaComboBox(telarefeicao.refeicao_opcao);
		ManipulaBotao1(telarefeicao.botao1);
		ManipulaBotao2(telarefeicao.botao2);
		
	}
	
	public void ManipulaComboBox(JComboBox<String> refeicao_opcao){
		refeicao_opcao.addActionListener(new ActionListener() {  
			  public void actionPerformed(ActionEvent evt) {  
			    
				String refeicao;
				  
				refeicao = telarefeicao.refeicao_opcao.getSelectedItem().toString();
				telarefeicao.area.setText(telarefeicao.area.getText()+ refeicao + "\n");
				
			  }  
			});  
		
	}
	
	public void ManipulaBotao1(JButton botao1){
		botao1.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				
				TelaMenu.area.setText(TelaMenu.area.getText() + telarefeicao.area.getText());
				//System.out.println(getListaPedido());
				telarefeicao.dispose();
			}});
		
	}
	
	
	public void ManipulaBotao2(JButton botao2){
	botao2.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent arg0) {

			telarefeicao.dispose();
			
		}});
	}

	public void leitor() throws IOException{
		
		
		BufferedReader buffRead = new BufferedReader(new FileReader("doc/estoquerefeicao.txt"));
		
		String linha = " ";
		String linhaaux = "";
		String[] dados;
		
		while(buffRead.ready()){
		
			if(!linhaaux.equals(linha)){
				linhaaux = buffRead.readLine();
				dados = linhaaux.split(";");
				
				telarefeicao.refeicao_opcao.addItem(dados[0]);
				
			}
			else{
				
				break;
			}
			
		}
		
		buffRead.close();
		
		
	}
	
}
