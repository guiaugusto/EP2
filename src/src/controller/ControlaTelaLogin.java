package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import view.TelaLogin;
import model.Servidor;

public class ControlaTelaLogin extends Servidor{

	public TelaLogin telalogin;
	public Servidor servidor;
	
	public ControlaTelaLogin(){
		
		servidor = new Servidor();
		telalogin = new TelaLogin();
		ManipulaBotao1(telalogin.botao1);
		ManipulaBotao2(telalogin.botao2);
		ManipulaBotao3(telalogin.botao3);
		
	}
	

	public void ManipulaBotao1(JButton botao1){

		botao1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
			try {
				leitor();
			} catch (IOException e) {
				e.printStackTrace();
			}	
	
			}
		
		});
	
	}

	public void ManipulaBotao2(JButton botao2){
		botao2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				telalogin.dispose();
			}	
		});
	}

	public void ManipulaBotao3(JButton botao3){
		botao3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				new ControlaCadastroServidor();
				telalogin.dispose();
				telalogin.Campo_login.setText("");
				telalogin.Campo_senha.setText("");
			}
		});
	}
	
@SuppressWarnings("deprecation")
public void leitor() throws IOException{
		
	
		BufferedReader buffRead = new BufferedReader(new FileReader("doc/registroservidor.txt"));
		setLogin(telalogin.Campo_login.getText());
		
		setSenha(telalogin.Campo_senha.getText());
		String linha = getLogin() + " " + getSenha();
		String linhaaux = "";
		
		while(true){
		
			if(!linhaaux.equals(linha)){
				linhaaux = buffRead.readLine();
				if(linhaaux == null){
					JOptionPane.showMessageDialog(null,"Login não existente!","Login não existente!" , JOptionPane.ERROR_MESSAGE);
					break;
				}
					
			}
			else{
				new ControlaTelaMenu();
				telalogin.dispose();
				break;
			}
			
		}
		
		buffRead.close();
		
		
	}
	
}