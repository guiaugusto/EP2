package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import view.TelaCadastroServidor;
import controller.ControlaTelaLogin;
import model.Servidor;

public class ControlaCadastroServidor extends Servidor{

	public TelaCadastroServidor telaservidor;
	public Servidor servidor;
	private ControlaTelaLogin controlalogin;
	
	public ControlaCadastroServidor(){
	
		servidor = new Servidor();
		telaservidor = new TelaCadastroServidor();
		ManipulaBotao1(telaservidor.botao1);
		ManipulaBotao2(telaservidor.botao2);
	}
	
	public void ManipulaBotao1(JButton botao1){
	
	botao1.addActionListener(new ActionListener(){
		
		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent arg0){
			
		int verifica = 1;
			
			
		if (!telaservidor.Campo_nome.getText().equals("") && !telaservidor.Campo_login.getText().equals("")){
			
			if(!telaservidor.Campo_senha.getText().equals("") && !telaservidor.Campo_confirma_senha.getText().equals("")){
				
				if(telaservidor.Campo_senha.getText().equals( telaservidor.Campo_confirma_senha.getText())){
				
					BufferedWriter buffWrite;
					try {
						buffWrite = new BufferedWriter(new FileWriter("doc/registroservidor.txt",true));
						escrever(buffWrite, telaservidor.Campo_senha, telaservidor.Campo_login);
						verifica = leitor();
						if(verifica == 0){
							telaservidor.dispose();
							new ControlaTelaLogin();
						}
						telaservidor.Campo_senha.setText("");
						telaservidor.Campo_confirma_senha.setText("");
						telaservidor.Campo_login.setText("");
							
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				}
				else{
					JOptionPane.showMessageDialog(null,"A senha confirmada não corresponde a anterior, insira novamente!","Senha incorreta" , JOptionPane.ERROR_MESSAGE);
					//Opção de senha diferente
				}
			}
			else{
				JOptionPane.showMessageDialog(null,"Campo senha e/ou confirmar senha não inserido!","Campo inválido" , JOptionPane.ERROR_MESSAGE);
			}
			
		}
		else{
			JOptionPane.showMessageDialog(null,"Campo(s) não preenchido(s)", "Campo(s) não preenchido(s)" , JOptionPane.ERROR_MESSAGE);
			//Opção de usuario não escreveu login ou senha
		}
		
		}
	});
	
}
	
	public void ManipulaBotao2(JButton botao2){
	botao2.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent arg0) {
			telaservidor.dispose();
			new ControlaTelaLogin();
		}});
	}

@SuppressWarnings("deprecation")
public void escrever(BufferedWriter buffWrite, JPasswordField Campo_senha, JTextField Campo_login) throws IOException{
		
		setLogin(telaservidor.Campo_login.getText());
		setSenha(telaservidor.Campo_senha.getText());
		String linha = getLogin() + " " + getSenha();
		
		buffWrite.append(linha + "\n");
		buffWrite.close();
		
	}

@SuppressWarnings({ "deprecation", "resource" })
public int leitor() throws IOException{
	
	BufferedReader buffRead = new BufferedReader(new FileReader("doc/registroservidor.txt"));
	setLogin(telaservidor.Campo_login.getText());
	
	setSenha(telaservidor.Campo_senha.getText());
	String linha = getLogin() + " " + getSenha();
	String linhaaux = "";
	
	while(true){
	
		if(!linhaaux.equals(linha)){
			linhaaux = buffRead.readLine();
			String logsenha[] = new String[2];
			logsenha = linhaaux.split(" ");
			
			if(logsenha[0].equals(getLogin())){
				JOptionPane.showMessageDialog(null,"Login já existente!", "Login já existente", JOptionPane.ERROR_MESSAGE);	
				return 1;
		}
			
		else{
			buffRead.close();
			return 0;
			
		}
	}
	
}
	
	
	
}

	
}
