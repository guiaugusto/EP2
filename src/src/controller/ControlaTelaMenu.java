package controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import view.TelaMenu;
import model.Estoque;


public class ControlaTelaMenu extends Estoque{

	public TelaMenu telamenu;
	public ControlaSelecaoRefeicao telarefeicao;
	public ControlaSelecaoBebida telabebida;
	public ControlaSelecaoSobremesa telasobremesa;
	public ControlaTelaLogin telalogin;
	public ControlaTelaEstoque telaestoque;
	public ControlaTelaObservacao telaobservacao;
	public ControlaTelaPagamento telapagamento;
	public String texto;
	
	public ControlaTelaMenu(){
		
		telamenu = new TelaMenu();
		opcao_refeicao(telamenu.botao_refeicao);
		opcao_bebida(telamenu.botao_bebidas);
		opcao_sobremesa(telamenu.botao_sobremesas);
		ManipulaBotao1(telamenu.botao1);
		ManipulaBotao2(telamenu.botao2);
		ManipulaBotao3(telamenu.botao3);
		
	}
	
	public void opcao_refeicao(JButton botao_refeicao){
		
		botao_refeicao.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
		
				telarefeicao = new ControlaSelecaoRefeicao();
				
			}
		});
		
	}
	
	public void opcao_bebida(JButton botao_bebida){
		
		botao_bebida.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
		
				telabebida = new ControlaSelecaoBebida();
				
			}
		});
		
	}

	public void opcao_sobremesa(JButton botao_sobremesa){
		
		botao_sobremesa.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
		
				telasobremesa = new ControlaSelecaoSobremesa();
				
			}
		});
		
	}
	
	public void ManipulaBotao1(JButton botao1){
		
		botao1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
		
				if(telamenu.area.getText().equals("")){
					JOptionPane.showMessageDialog(null,"Item não selecionado","Nenhum item selecionado" , JOptionPane.ERROR_MESSAGE);
				}
				else{
					 if(JOptionPane.showConfirmDialog(null,"Deseja fazer alguma observação?","Aviso",JOptionPane.YES_NO_OPTION) == 1){
						 telapagamento = new ControlaTelaPagamento();
					 }
					 else{
						 
						 telaobservacao = new ControlaTelaObservacao();
					 }
				}
				
			}
		});
		
	}

	public void ManipulaBotao2(JButton botao2){
		
		botao2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
		
				telalogin = new ControlaTelaLogin();
				telamenu.dispose();
				
			}
		});
		
	}
	
	public void ManipulaBotao3(JButton botao3){
		
		botao3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
		
				telaestoque = new ControlaTelaEstoque();
				
			}
		});
		
	}
	


	
}
