package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import model.Estoque;

import view.TelaSelecaoBebida;
import view.TelaSelecaoSobremesa;

public class ControlaSelecaoBebida extends Estoque{

	public TelaSelecaoBebida telabebida;
	
	public ControlaSelecaoBebida(){
		
		telabebida = new TelaSelecaoBebida();

		try {
			leitor();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ManipulaComboBox(telabebida.bebida_opcao);
		ManipulaBotao1(telabebida.botao1);
		ManipulaBotao2(telabebida.botao2);

		}

public void ManipulaComboBox(JComboBox<String> bebida_opcao){
	bebida_opcao.addActionListener(new ActionListener() {  
		  public void actionPerformed(ActionEvent evt) {  
		    
			String refeicao;
			  
			refeicao = telabebida.bebida_opcao.getSelectedItem().toString();
			telabebida.area.setText(telabebida.area.getText()+ refeicao + "\n");
			
		  }  
		});  
	
}

public void ManipulaBotao1(JButton botao1){
	botao1.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent arg0) {
		
			setListaPedido(telabebida.area.getText());
			telabebida.dispose();
		}});
	
}


public void ManipulaBotao2(JButton botao2){
botao2.addActionListener(new ActionListener(){

	public void actionPerformed(ActionEvent arg0) {

		telabebida.dispose();
		
	}});
}

public void leitor() throws IOException{
	
	
	BufferedReader buffRead = new BufferedReader(new FileReader("doc/estoquebebidas.txt"));
	
	String linha = " ";
	String linhaaux = "";
	String[] dados;
	
	while(buffRead.ready()){
	
		if(!linhaaux.equals(linha)){
			linhaaux = buffRead.readLine();
			dados = linhaaux.split(";");
			
			telabebida.bebida_opcao.addItem(dados[0]);
			
		}
		else{
			
			break;
		}
		
	}
	
	buffRead.close();
	
	
}

	
}
