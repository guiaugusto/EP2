package view;

import java.awt.Container;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class TelaObservacoes extends JFrame{

	public JTextArea campo_texto;
	//public DefaultTableModel model;
	public JScrollPane scroll;
	
	public TelaObservacoes(){
		
		super("Observações");
		
		Container componente = getContentPane();	
		
		componente.setLayout(null);
		
			String nome_opcoes = "Observações";
			
			JLabel headerlabel = new JLabel(nome_opcoes);
	
			headerlabel.setLocation(120, 20);
			headerlabel.setFont(new Font(nome_opcoes, Font.BOLD, 22));
			//Colocar cor posteriormente
			headerlabel.setSize(500, 60);
		
		add(headerlabel);
		
		campo_texto = new JTextArea();
		
		scroll = new JScrollPane(campo_texto);
	    scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		campo_texto.setSize(300, 300);
		campo_texto.setLocation(50, 100);
		add(campo_texto);
		add(scroll);
		campo_texto.setLineWrap(true);
		
		JButton botao1 = new JButton("Finalizar");
		JButton botao2 = new JButton("Voltar");
		
		botao1.setSize(120, 30);
		botao1.setLocation(50, 425);
		
		botao2.setSize(120, 30);
		botao2.setLocation(230, 425);
		
		add(botao1);
		add(botao2);
		
		setSize(400, 500);
		setVisible(true);
		setResizable(false);
		
	}
	
}
