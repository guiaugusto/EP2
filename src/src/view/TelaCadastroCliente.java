package view;

import java.awt.Container;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class TelaCadastroCliente extends JFrame{
	
	private JLabel texto_cliente;
	private JLabel texto_cpf;
	private JLabel texto_endereco;
	private JLabel texto_telefone;
	public JButton botao1;
	public JButton botao2;
	public JTextField Campo_nome_cliente;
	public JTextField Campo_cpf;
	public JTextField Campo_endereco;
	public JTextField Campo_telefone;
	
	
	public TelaCadastroCliente(){
	
	super("Cadastro do Cliente");
	
	Container componente = getContentPane();	
	
	componente.setLayout(null);
	
	String nome_do_cliente = "Nome do cliente";
	String cpf = "CPF";
	String endereco = "Endereço";
	String telefone = "Telefone";
	
	String nome_opcoes = "Cadastro de Cliente";
	
	JLabel headerlabel = new JLabel(nome_opcoes);

	headerlabel.setLocation(170, 20);
	headerlabel.setFont(new Font(nome_opcoes, Font.BOLD, 22));
	headerlabel.setSize(500, 60);
	
	texto_cliente = new JLabel(nome_do_cliente);
	texto_cpf = new JLabel(cpf);
	texto_endereco = new JLabel(endereco);
	texto_telefone = new JLabel(telefone);
	
	texto_cliente.setSize(300, 30);
	texto_cliente.setLocation(30,100);

	texto_cpf.setSize(100, 30);
	texto_cpf.setLocation(50,150);
	
	texto_endereco.setSize(100, 30);
	texto_endereco.setLocation(50, 200);
	
	texto_telefone.setSize(100, 30);
	texto_telefone.setLocation(50, 250);
	
	add(headerlabel);
	add(texto_cliente);
	add(texto_cpf);
	add(texto_endereco);
	add(texto_telefone);
	
	botao1 = new JButton("Confirmar");
	botao2 = new JButton("Voltar");
	
	botao1.setSize(120, 30);
	botao1.setLocation(100,320);
	
	botao2.setSize(120, 30);
	botao2.setLocation(380,320);
	
	add(botao1);
	add(botao2);
	
	Campo_nome_cliente = new JTextField();
	Campo_cpf = new JTextField();
	Campo_endereco = new JTextField();
	Campo_telefone = new JTextField();
	
	Campo_nome_cliente.setSize(350, 30);
	Campo_nome_cliente.setLocation(180,100);
	
	Campo_cpf.setSize(350, 30);
	Campo_cpf.setLocation(180, 150);
	
	Campo_endereco.setSize(350, 30);
	Campo_endereco.setLocation(180, 200);
	
	Campo_telefone.setSize(350, 30);
	Campo_telefone.setLocation(180, 250);
	
	add(Campo_nome_cliente);
	add(Campo_cpf);
	add(Campo_endereco);
	add(Campo_telefone);
	
	setSize(600, 400);
	setResizable(false);
	setVisible(true);
		
	}
	
}
