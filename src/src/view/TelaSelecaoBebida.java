package view;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

public class TelaSelecaoBebida extends JFrame{

	public JButton botao1;
	public JButton botao2;
	public JComboBox<String> bebida_opcao;
	public static JTextArea area;
	public DefaultTableModel model;
	public JScrollPane scroll;
	
	public TelaSelecaoBebida(){
		
		super("Seleção de Bebidas");
		
		Container componente = getContentPane();	
		
		componente.setLayout(null);
		
		botao1 = new JButton("Confirmar");	
		botao2 = new JButton("Cancelar");
		
		botao1.setSize(120, 30);
		botao2.setSize(120, 30);
		
		botao1.setLocation(20,250);
		botao2.setLocation(160,250);
	
		add(botao1);
		add(botao2);
		
		bebida_opcao = new JComboBox<String>();
		bebida_opcao.setSize(200, 30);
		bebida_opcao.setLocation(50, 50);
		add(bebida_opcao);
		
		
		area = new JTextArea();
		
		//table.getColumnModel().getColumn(0).setResizable(false);		
		//table.getColumnModel().getColumn(0).setPreferredWidth(30);
		
		scroll = new JScrollPane(area);
	    scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		scroll.setSize(220, 100);
		scroll.setLocation(40, 110);
		

		area.setEditable(false);
		area.setLineWrap(true);
		
		add(scroll);
		
		setSize(300, 300);
		setVisible(true);
		setResizable(false);
		

	}
	
}
