package view;

import java.awt.Font;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;


@SuppressWarnings("serial")
public class TelaMenu extends JFrame{
	
	public static JTextArea area;
	public JScrollPane scroll;
	public JButton botao1;
	public JButton botao2;
	public JButton botao3;
	public ImageIcon refeicao_imagem;
	public ImageIcon bebidas_imagem;
	public ImageIcon sobremesas_imagem;
	public JButton botao_refeicao;
	public JButton botao_bebidas;
	public JButton botao_sobremesas;
	private JLabel headerlabel;
	private JLabel texto_pedido;
	
	public TelaMenu(){
		
		GroupLayout layout = new GroupLayout(getContentPane());
		
		setLayout(layout);
	   
		String nome_menu = "Menu";
		String nome_pedido = "Pedido";
		
		headerlabel = new JLabel(nome_menu);
		texto_pedido = new JLabel(nome_pedido);
		
		headerlabel.setSize(500, 60);
		headerlabel.setFont(new Font(nome_menu, Font.BOLD, 22));
		headerlabel.setLocation(50,10);
		
		texto_pedido.setSize(500, 30);
		texto_pedido.setFont(new Font(nome_pedido, Font.ROMAN_BASELINE, 18));
		texto_pedido.setLocation(25,125);
		
		add(headerlabel);
		add(texto_pedido);
		
		area = new JTextArea(); 

		scroll = new JScrollPane(area);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setSize(400,200);
		scroll.setLocation(50, 200);

		area.setEditable(false);
		area.setLineWrap(true);

		
		add(scroll);
		

		//Botões
		
		botao1 = new JButton("Confirmar");
		botao2 = new JButton("Cancelar");
		botao3 = new JButton("Adicionar produtos ao estoque");
		
		refeicao_imagem = new ImageIcon("doc/refeicao.jpg");
		refeicao_imagem.setImage(refeicao_imagem.getImage().getScaledInstance(75, 75, 100));
		bebidas_imagem = new ImageIcon("doc/bebidas.png");
		bebidas_imagem.setImage(bebidas_imagem.getImage().getScaledInstance(75, 75, 100));
		sobremesas_imagem = new ImageIcon("doc/sobremesas.jpg");
		sobremesas_imagem.setImage(sobremesas_imagem.getImage().getScaledInstance(75, 75, 100));
		
		botao_refeicao = new JButton(refeicao_imagem);
		botao_bebidas = new JButton(bebidas_imagem);
		botao_sobremesas = new JButton(sobremesas_imagem);
		
		botao_refeicao.setSize(75,75);
		botao_refeicao.setLocation(120, 100);
		
		botao_bebidas.setSize(75,75);
		botao_bebidas.setLocation(245, 100);
		
		botao_sobremesas.setSize(75,75);
		botao_sobremesas.setLocation(370, 100);
		
		add(botao_refeicao);
		add(botao_bebidas);
		add(botao_sobremesas);
		
		botao1.setSize(120, 30);
		botao1.setLocation(100,475);
		
		botao2.setSize(120, 30);
		botao2.setLocation(280,475);
		
		botao3.setSize(300, 30);
		botao3.setLocation(100,515);
		
		add(botao1);
		add(botao2);
		add(botao3);
		
		setResizable(false);
		setSize(500,600);
		setVisible(true);
	}
	
	
}
