package view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TelaEstoque extends JFrame{

	private JLabel headerlabel;
	public JButton botao1;
	public JButton botao2;
	public JButton botao3;
	public JTable table;
	public JScrollPane scroll;
	public JList<String> lista;
	public DefaultListModel<String> model;
	
	public TelaEstoque(){
	
		super("Adicionar estoque");
		
		String nome_estoque = "Estoque";
		
		Container componente = getContentPane();	
		
		componente.setLayout(null);
		
		headerlabel = new JLabel("Estoque");
		
		headerlabel.setLocation(200, 10);
		headerlabel.setFont(new Font(nome_estoque, Font.BOLD, 22));
		//Colocar cor posteriormente
		headerlabel.setSize(500, 60);
		
		add(headerlabel);
		
		model = new DefaultListModel<String>();
		lista = new JList<String>(model);
		scroll = new JScrollPane(lista);
		lista.setVisibleRowCount(3);
		scroll.setPreferredSize(new Dimension(20, 20));
		scroll.setSize(400, 100);
		scroll.setLocation(50,75);
		
		add(scroll);
		
		botao1 = new JButton("Confirmar");
		botao2 = new JButton("Voltar");
		botao3 = new JButton("Adicionar novo item");
		
		botao1.setSize(120, 30);
		botao1.setLocation(80, 200);
		
		botao2.setSize(120, 30);
		botao2.setLocation(310, 200);
		
		botao3.setSize(350, 30);
		botao3.setLocation(80, 250);
		
		add(botao1);
		add(botao2);
		add(botao3);
		
		setSize(500, 300);
		setResizable(false);
		setVisible(true);
	}
		
	
}
