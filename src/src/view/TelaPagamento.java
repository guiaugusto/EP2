package view;

import java.awt.Container;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

public class TelaPagamento extends JFrame{

	public JLabel headerlabel;
	
	public TelaPagamento(){
		
		super("Pagamento");
		
		Container componente = getContentPane();	
		
		componente.setLayout(null);

		String nome_pagamento = "Pagamento";
		
		headerlabel = new JLabel(nome_pagamento);
		
		headerlabel.setLocation(125, 10);
		headerlabel.setFont(new Font(nome_pagamento, Font.BOLD, 22));
		//Colocar cor posteriormente
		headerlabel.setSize(500, 60);
		
		add(headerlabel);
		
		String texto_cartao = "Pagamento em dinheiro";
		String texto_dinheiro = "Pagamento em cartão";
		
		JRadioButton selecao1 = new JRadioButton(texto_cartao);
		JRadioButton selecao2 = new JRadioButton(texto_dinheiro);
		
		selecao1.setFont(new Font(texto_cartao, Font.CENTER_BASELINE, 15));
		selecao2.setFont(new Font(texto_dinheiro, Font.CENTER_BASELINE, 15));
		
		selecao1.setSize(300, 40);
		selecao1.setLocation(50, 80);
		
		selecao2.setSize(300, 50);
		selecao2.setLocation(50, 120);
	
		ImageIcon visa = new ImageIcon("doc/visa.png");
		visa.setImage(visa.getImage().getScaledInstance(75, 75, 100));
		ImageIcon mastercard = new ImageIcon("doc/mastercard.png");
		mastercard.setImage(mastercard.getImage().getScaledInstance(75, 75, 100));
		ImageIcon elo = new ImageIcon("doc/elo.jpg");
		elo.setImage(elo.getImage().getScaledInstance(75, 75, 100));
				
		JButton bandeira_visa = new JButton(visa);
		JButton bandeira_master = new JButton(mastercard);
		JButton bandeira_elo = new JButton(elo);
		
		bandeira_visa.setSize(75,50);
		bandeira_master.setSize(75,50);
		bandeira_elo.setSize(75,50);
		
		bandeira_visa.setLocation(60,175);
		bandeira_master.setLocation(160, 175);
		bandeira_elo.setLocation(260, 175);
		
		add(bandeira_visa);
		add(bandeira_master);
		add(bandeira_elo);
		
		add(selecao1);
		add(selecao2);
		
		JLabel pagamentolabel = new JLabel("Pagamento: ");
		JLabel valorlabel = new JLabel("Valor: ");
		JLabel nomelabel = new JLabel("Cliente: ");
		JLabel cpf = new JLabel("CPF: ");
		JLabel endereco = new JLabel("Endereço: ");
		
		pagamentolabel.setSize(300,30);
		pagamentolabel.setLocation(50, 250);
		
		valorlabel.setSize(300,30);
		valorlabel.setLocation(50, 280);
		
		nomelabel.setSize(300, 30);
		nomelabel.setLocation(50, 310);
		
		cpf.setSize(300, 30);
		cpf.setLocation(50, 340);
		
		endereco.setSize(300, 30);
		endereco.setLocation(50, 370);
		
		add(pagamentolabel);
		add(valorlabel);
		add(cpf);
		add(nomelabel);
		add(endereco);
		
		JButton botao1 = new JButton("Finalizar");
		JButton botao2 = new JButton("Voltar");
		
		botao1.setSize(120, 30);
		botao2.setSize(120, 30);
		
		botao1.setLocation(50 ,430);
		botao2.setLocation(230,430);
		
		add(botao1);
		add(botao2);
		
		setSize(400, 500);
		setResizable(false);
		setVisible(true);
		
	}
	
}
