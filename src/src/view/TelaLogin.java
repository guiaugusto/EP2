package view;

import java.awt.*;

import javax.swing.*;


@SuppressWarnings("serial")
public class TelaLogin extends JFrame{

	public JTextField Campo_login;
	public JPasswordField Campo_senha;
	public JButton botao1;
	public JButton botao2;
	public JButton botao3;
	public JLabel headerlabel;
	public JLabel option1;
	public JLabel option2;

	public TelaLogin(){		
		
		super("Tô com fome, quero mais!");
		
		Container componente = getContentPane();	
		
		componente.setLayout(null);
		
		//Área de escrita e	inserção de login e senha
		
		//Área de escrita
		
		String nome_restaurante = "Tô com fome, quero mais!";
		String texto_login = "Login";
		String texto_senha = "Senha";
				
		headerlabel = new JLabel(nome_restaurante);
		option1 = new JLabel(texto_login);
		option2 = new JLabel(texto_senha);
		
		headerlabel.setLocation(80, 30);
		headerlabel.setFont(new Font(nome_restaurante, Font.BOLD, 22));
		//Colocar cor posteriormente
		headerlabel.setSize(500, 60);
		
		option1.setLocation(101, 105);
		option1.setFont(new Font(texto_login, Font.BOLD, 14));
		//Colocar cor posteriormente
		option1.setSize(500, 100);
		
		option2.setLocation(100, 145);
		option2.setFont(new Font(texto_senha, Font.BOLD, 14	));
		//Colocar cor posteriormente
		option2.setSize(500, 100);
		
		componente.add(headerlabel);
		componente.add(option1);
		componente.add(option2);
		
		//Área de inserção
		Campo_login = new JTextField();
		Campo_senha = new JPasswordField();
		
		Campo_login.setLocation(170, 140);
		Campo_login.setSize(250, 30);
		
		Campo_senha.setLocation(170, 180);
		Campo_senha.setSize(250, 30);
		
		componente.add(Campo_login);
		componente.add(Campo_senha);
		
		//Área dos botões da tela de login
		
		botao1 = new JButton("Confirmar");
		botao2 = new JButton("Sair");
		botao3 = new JButton("Cadastrar");		
		
		botao1.setLocation(40, 290);
		botao2.setLocation(180, 290);
		botao3.setLocation(320, 290);	
		
		botao1.setSize(120, 30);
		botao2.setSize(120, 30);
		botao3.setSize(120, 30);
		
		componente.add(botao1);
		componente.add(botao2);
		componente.add(botao3);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 400);
		setVisible(true);
		setResizable(false);
	}
	
	

}
