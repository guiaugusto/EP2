package view;

import java.awt.Font;
//import java.io.BufferedReader;

import javax.swing.*;


@SuppressWarnings("serial")
public class TelaCadastroServidor extends JFrame{
	
	public JTextField Campo_nome;
	public JPasswordField Campo_confirma_senha;
	public JTextField Campo_login;
	public JPasswordField Campo_senha;
	public JButton botao1;
	public JButton botao2;
	
	public TelaCadastroServidor(){
		
	setLayout(null);
	
	String nome_restaurante = "Cadastro do Servidor";
	String texto_login = "Login";
	String texto_senha = "Senha";
	String texto_nome = "Nome";
	String texto_confirmar_senha = "Confirmar senha";
			
	JLabel headerlabel = new JLabel(nome_restaurante);
	JLabel option1 = new JLabel(texto_login);
	JLabel option2 = new JLabel(texto_senha);
	JLabel option3 = new JLabel(texto_confirmar_senha);
	JLabel option4 = new JLabel(texto_nome);
	
	headerlabel.setLocation(125, 30);
	headerlabel.setFont(new Font(nome_restaurante, Font.BOLD, 22));
	//Colocar cor posteriormente
	headerlabel.setSize(500, 60);

	option4.setLocation(50, 105);
	option4.setFont(new Font(texto_senha, Font.ROMAN_BASELINE, 14));
	//Colocar cor posteriormente
	option4.setSize(500, 100);
	
	option1.setLocation(50, 145);
	option1.setFont(new Font(texto_login, Font.ROMAN_BASELINE, 14));
	//Colocar cor posteriormente
	option1.setSize(500, 100);
	
	option2.setLocation(50, 185);
	option2.setFont(new Font(texto_senha, Font.ROMAN_BASELINE, 14));
	//Colocar cor posteriormente
	option2.setSize(500, 100);

	option3.setLocation(20, 225);
	option3.setFont(new Font(texto_senha, Font.ROMAN_BASELINE, 14));
	//Colocar cor posteriormente
	option3.setSize(500, 100);

	add(headerlabel);
	add(option1);
	add(option2);
	add(option3);
	add(option4);
	
	//Área de inserção
	
	Campo_nome = new JTextField();
	Campo_senha = new JPasswordField();
	Campo_login = new JTextField();
	Campo_confirma_senha = new JPasswordField();
	
	Campo_nome.setLocation(150, 140);
	Campo_nome.setSize(300, 30);
	
	Campo_login.setLocation(150, 180);
	Campo_login.setSize(300, 30);
	
	Campo_senha.setLocation(150, 220);
	Campo_senha.setSize(300, 30);
	
	Campo_confirma_senha.setLocation(150, 260);
	Campo_confirma_senha.setSize(300, 30);
	
	add(Campo_nome);
	add(Campo_login);
	add(Campo_senha);
	add(Campo_confirma_senha);
	
	//Área do botão
	
	botao1 = new JButton("Confirmar");
	botao2 = new JButton("Cancelar");
	
	botao1.setLocation(120, 330);
	botao2.setLocation(260, 330);
	
	botao1.setSize(120, 30);
	botao2.setSize(120, 30);
	
	add(botao1);
	add(botao2);

	setResizable(false);
	setSize(600, 430);
	setVisible(true);	
	
	}
	
		
}
	