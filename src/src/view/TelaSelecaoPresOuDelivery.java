package view;

import java.awt.Container;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

public class TelaSelecaoPresOuDelivery extends JFrame{

	public TelaSelecaoPresOuDelivery(){
	
		super("Tipo de venda");
		
		Container componente = getContentPane();	
		
		componente.setLayout(null);
		
		String tipo_venda = "Tipo de venda";
		
		JLabel headerlabel = new JLabel(tipo_venda);
		
		headerlabel.setSize(300,30);
		headerlabel.setFont(new Font(tipo_venda, Font.BOLD, 22));
		headerlabel.setLocation(50, 20);
		
		add(headerlabel);
		
		JRadioButton opcao1 = new JRadioButton("Delivery");
		JRadioButton opcao2 = new JRadioButton("Presencial");
		
		opcao1.setSize(300, 30);
		opcao1.setLocation(50, 60);
		
		opcao2.setSize(300, 30);
		opcao2.setLocation(50, 110);
		
		add(opcao1);
		add(opcao2);
		
		setSize(300,200);
		setVisible(true);
		setResizable(false);
	}
	
}
